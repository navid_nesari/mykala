<?php

use App\Http\Controllers\Auth\admin\ForgetPasswordController;
use App\Http\Controllers\Auth\admin\LoginController;
use App\Http\Controllers\Auth\admin\SocialLoginController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => ''], function() {
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('auth.admin.login.form');
    Route::post('/login', [LoginController::class, 'login'])->name('auth.admin.login')->middleware('throttle:userLimit');
    Route::get('/logout', [LoginController::class, 'logout'])->name('auth.admin.logout');

    Route::get('forget-password', [ForgetPasswordController::class, 'showForgetPasswordForm'])->name('auth.admin.forget-password.form');
    Route::post('forget-password', [ForgetPasswordController::class, 'submitForgetPassword'])->name('auth.admin.forget-password');
    Route::get('reset-password/{token}', [ForgetPasswordController::class, 'showResetPasswordForm'])->name('auth.admin.reset-password.form');
    Route::post('reset-password', [ForgetPasswordController::class, 'submitResetPassword'])->name('auth.admin.reset-password');

    Route::get('/google/redirect', [SocialLoginController::class, 'redirectToProvider'])->name('auth.admin.google.redirect');
    Route::get('/google/callback', [SocialLoginController::class, 'handleProviderCallback']);
});
