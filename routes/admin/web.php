<?php

use App\Http\Controllers\Admin\ArticlesController;
use App\Http\Controllers\Admin\CommentsController;
use App\Http\Controllers\Admin\CoursesController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\EpisodesController;
use App\Http\Controllers\Admin\LevelAdminsController;
use App\Http\Controllers\Admin\PaymentsController;
use App\Http\Controllers\Admin\PermissionsController;
use App\Http\Controllers\Admin\RolesController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\UsersController;
use Illuminate\Support\Facades\Route;


Route::group(['prefix' => '', 'middleware' => ['auth.admin', 'activeUser']], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    // articles
    Route::group(['prefix' => 'articles'], function () {
        Route::get('/', [ArticlesController::class, 'all'])->name('admin.articles.all');
        Route::get('/create', [ArticlesController::class, 'create'])->name('admin.articles.create');
        Route::post('/store', [ArticlesController::class, 'store'])->name('admin.articles.store');
        Route::get('/edit/{article}', [ArticlesController::class, 'edit'])->name('admin.articles.edit');
        Route::post('/update/{article}', [ArticlesController::class, 'update'])->name('admin.articles.update');
        Route::get('/delete/{article}', [ArticlesController::class, 'delete'])->name('admin.articles.delete');
        Route::post('/', [ArticlesController::class, 'uploadImageWithCkeditor'])->name('admin.articles.uploadImageWithCkeditor');
    });

    // courses
    Route::group(['prefix' => 'courses'], function () {
        Route::get('/', [CoursesController::class, 'all'])->name('admin.courses.all');
        Route::get('/create', [CoursesController::class, 'create'])->name('admin.courses.create');
        Route::post('/store', [CoursesController::class, 'store'])->name('admin.courses.store');
        Route::get('/edit/{course}', [CoursesController::class, 'edit'])->name('admin.courses.edit');
        Route::post('/update/{course}', [CoursesController::class, 'update'])->name('admin.courses.update');
        Route::get('/delete/{course}', [CoursesController::class, 'delete'])->name('admin.courses.delete');
    });

    // episodes
    Route::group(['prefix' => 'episodes'], function () {
        Route::get('/', [EpisodesController::class, 'all'])->name('admin.episodes.all');
        Route::get('/create', [EpisodesController::class, 'create'])->name('admin.episodes.create');
        Route::post('/store', [EpisodesController::class, 'store'])->name('admin.episodes.store');
        Route::get('/edit/{episode}', [EpisodesController::class, 'edit'])->name('admin.episodes.edit');
        Route::post('/update/{episode}', [EpisodesController::class, 'update'])->name('admin.episodes.update');
        Route::get('/delete/{episode}', [EpisodesController::class, 'delete'])->name('admin.episodes.delete');
    });

    // roles
    Route::group(['prefix' => 'roles'], function () {
        Route::get('/', [RolesController::class, 'all'])->name('admin.roles.all');
        Route::get('/create', [RolesController::class, 'create'])->name('admin.roles.create');
        Route::post('/store', [RolesController::class, 'store'])->name('admin.roles.store');
        Route::get('/edit/{role}', [RolesController::class, 'edit'])->name('admin.roles.edit');
        Route::post('/update/{role}', [RolesController::class, 'update'])->name('admin.roles.update');
        Route::get('/delete/{role}', [RolesController::class, 'delete'])->name('admin.roles.delete');
    });

    // permissions
    Route::group(['prefix' => 'permissions'], function () {
        Route::get('/', [PermissionsController::class, 'all'])->name('admin.permissions.all');
        Route::get('/create', [PermissionsController::class, 'create'])->name('admin.permissions.create');
        Route::post('/store', [PermissionsController::class, 'store'])->name('admin.permissions.store');
        Route::get('/edit/{permission}', [PermissionsController::class, 'edit'])->name('admin.permissions.edit')->middleware('can:show-users,show-comment,edit-article');
        Route::post('/update/{permission}', [PermissionsController::class, 'update'])->name('admin.permissions.update');
        Route::get('/delete/{permission}', [PermissionsController::class, 'delete'])->name('admin.permissions.delete');
    });

    // users
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UsersController::class, 'all'])->name('admin.users.all');
        Route::get('/delete/{user}', [UsersController::class, 'delete'])->name('admin.users.delete');

        // levels
        Route::group(['prefix' => 'levels'], function () {
            Route::get('/', [LevelAdminsController::class, 'all'])->name('admin.levels.all');
            Route::get('/create', [LevelAdminsController::class, 'create'])->name('admin.levels.create');
            Route::post('/store', [LevelAdminsController::class, 'store'])->name('admin.levels.store');
            Route::get('/edit/{level}', [LevelAdminsController::class, 'edit'])->name('admin.levels.edit');
            Route::post('/update/{level}', [LevelAdminsController::class, 'update'])->name('admin.levels.update');
            Route::get('/delete/{level}', [LevelAdminsController::class, 'delete'])->name('admin.levels.delete');
        });
    });

    // comments
    Route::group(['prefix' => 'comments'], function() {
        Route::get('/', [CommentsController::class, 'all'])->name('admin.comments.all');
        Route::get('/unsuccessful', [CommentsController::class, 'unsuccessful'])->name('admin.comments.unsuccessful');
        Route::post('/update/{comment}', [CommentsController::class, 'update'])->name('admin.comments.update');
        Route::get('/delete/{comment}', [CommentsController::class, 'delete'])->name('admin.comments.delete');
    });

    // payments
    Route::group(['prifix' => 'payments'], function() {
        Route::get('/', [PaymentsController::class, 'all'])->name('admin.payments.all');
        Route::get('/unsuccessful', [PaymentsController::class, 'unsuccessful'])->name('admin.payments.unsuccessful');
        Route::post('/update/{payment}', [PaymentsController::class, 'update'])->name('admin.payments.update');
        Route::get('/delete/{payment}', [PaymentsController::class, 'delete'])->name('admin.payments.delete');
    });

});


// user
Route::group(['prefix' => 'user'], function(){
    Route::get('/active/email/{token}', [UserController::class, 'activeToken'])->name('active.user');
});
