<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\v1\ArticleController;
use App\Http\Controllers\Api\v1\ArticlesController;
use App\Http\Controllers\Api\v1\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| نکات :
|--------------------------------------------------------------------------
|
| {user?} => زمانیکه در یو ار ال یا اندپوینت روت مان از پرامتر به همراه
|  علامت سوال استفاده می کنیم در واقع ما دو روت را مینویسیم به این صورت
|  که هم میتوان این روت را بدون پرامتر درخواست کرد هم با پارامتر
|  (all & edit یعنی به طور مثال ترکیب دو روت )
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'middleware' => 'auth:sanctum'], function() {
    Route::get('/user/{user?}', [UsersController::class, 'all']);
    Route::get('/search/{name}', [UsersController::class, 'search']);
    Route::post('/user', [UsersController::class, 'create']);
    Route::put('/user', [UsersController::class, 'update']);
    Route::delete('/user/delete/{user}', [UsersController::class, 'delete']);

    Route::post('/article', [ArticleController::class, 'store']);
});

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
