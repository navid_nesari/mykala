<?php

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home\FeedController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Home\CoursesController;
use App\Http\Controllers\Home\SitemapController;
use App\Http\Controllers\Home\ArticlesController;
use App\Http\Controllers\Home\PaymentsController;
use App\Http\Controllers\Home\TelegramController;

// Route::get('/articles', [ArticlesController, 'index']);
// Route::get('/courses', [CoursesController, 'index']);

Route::get('/articles/{articleSlug}', [ArticlesController::class, 'single'])->name('home.article.single');
Route::get('/courses/{course}', [CoursesController::class, 'single'])->name('home.course.single');

Route::post('/comments', [HomeController::class, 'comment'])->name('home.comments');

// payment gateway
Route::group(['middleware' => 'auth.admin'], function() {
    Route::post('/course/payment',[PaymentsController::class, 'zarinpalPayment'])->name('home.course.payment');
    Route::get('/course/payment/checker',[PaymentsController::class, 'zarinpalChecker'])->name('home.course.payment.checker');

    // Route::post('/course/payment',[PaymentsController::class, 'paypingPayment'])->name('home.course.payment');
    // Route::get('/course/payment/checker',[PaymentsController::class, 'paypingChecker'])->name('home.course.payment.checker');
});

Route::get('/sitemap', [SitemapController::class, 'index']);
Route::get('/sitemap-articles', [SitemapController::class, 'articles']);

Route::get('/feed/articles', [FeedController::class, 'articles']);

Route::get('/telegram', [TelegramController::class, 'telegram']);

Route::get('/', [HomeController::class, 'index']);
