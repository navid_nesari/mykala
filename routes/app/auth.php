<?php

use App\Http\Controllers\Auth\app\LoginController;
use App\Http\Controllers\Auth\app\RegisterController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => ''], function() {
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('auth.app.login.form');
    Route::post('/login', [LoginController::class, 'login'])->name('auth.app.login');
    Route::get('/logout', [LoginController::class, 'logout'])->name('auth.app.logout');

    Route::get('/register', [RegisterController::class, 'showRegisterForm'])->name('auth.app.register.form');
    Route::post('/register', [RegisterController::class, 'register'])->name('auth.app.register');

});
