<?php

use App\Events\ArticleEvent;
use App\Events\UserActivation;
use App\Events\UserRegistered;
use App\Http\Controllers\HomeController;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    // $user = User::find(11);
    // event(new UserActivation($user));
    /**
     * broadcast event
     * $user = auth()->user();
     * event(new ArticleEvent($user));
     * return auth()->user();
    */

    // return view('home.index');
})->name('home');


//ajax file uploade
Route::get('/ajax-file-uploade', [HomeController::class, 'ajaxFileUploade']);
Route::post('/ajax-file-uploade', [HomeController::class, 'ajaxImageUploadPost'])->name('ajax.file.upload');
