@extends('layouts.auth')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/css/sweetalert2.min.css')}}">
@endsection

@section('content')

    <!--begin::Card-->
    <div class="card rounded-3 w-md-550px">
        <!--begin::Card body-->
        <div class="card-body p-10 p-lg-20">
            <!--begin::Form-->
            <form class="form w-100"  action="{{route('auth.admin.reset-password')}}" method="post">
                @csrf
                <!--begin::Heading-->
                <div class="text-center mb-11">
                    <!--begin::Title-->
                    <h3 class="text-dark fw-bolder mb-3">تنظیم مجدد رمز عبور</h3>
                    <!--end::Title-->
                </div>
                <!--begin::Heading-->
                <!--begin::Alert-->
                @if (Session::get('fail'))
                    <div class="alert alert-danger">
                        {{ Session::get('fail') }}
                    </div>
                @endif
                <!--end::Alert-->
                <!--begin::Input group=-->
                <div class="fv-row mb-8">
                    <!--begin::Email-->
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input type="text" placeholder="ایمیل" name="email" class="form-control"
                        value="{{ $email ?? old('email') }}" />
                    <!--end::Email-->
                    @error('email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!--end::Input group=-->
                <div class="fv-row mb-8">
                    <!--begin::Password-->
                    <input type="password" placeholder="رمز عبور" name="password"
                        class="form-control" value="{{old('password')}}" />
                    <!--end::Password-->
                    @error('password')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!--end::Input group=-->
                <div class="fv-row mb-8">
                    <!--begin::Password-->
                    <input type="password" placeholder="تایید رمز عبور" name="password_confirmation"
                        class="form-control" value="{{old('password-confirmation')}}" />
                    <!--end::Password-->
                    @error('password')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!--end::Input group=-->

                <!--begin::Submit button-->
                <div class="d-grid mb-10">
                    <button type="submit" class="btn btn-primary">
                        <!--begin::Indicator label-->
                        <span class="indicator-label">بازیابی رمز عبور</span>
                        <!--end::Indicator label-->
                    </button>
                </div>
                <!--end::Submit button-->

            </form>
            <!--end::Form-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

@endsection

@section('script')
    <script src="{{asset('admin-assets/js/sweetalert2.min.js')}}"></script>
    <script>
        @if(session('success'))
        Swal.fire({
            icon: 'success',
            title: 'عملیات موفق',
            text: "{{session('success')}}",  // session('success') = with('success', '...')
            confirmButtonText: 'باشه'
        })
        @endif

        @if(session('warning'))
        Swal.fire({
            icon: 'warning',
            title: 'عملیات ناموفق',
            text: "{{session('warning')}}",  // session('success') = with('success', '...')
            confirmButtonText: 'باشه'
        })
        @endif
    </script>
@endsection
