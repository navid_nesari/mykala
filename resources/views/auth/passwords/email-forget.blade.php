{{-- <h1>forget password Email</h1>
<p>{!! $body !!}</p>
<br>
<a href="{{ $reset_link }}">Reset Password</a> --}}
@component('mail::message')
# ایمیل بازیابی رمز عبور

برای تنظیم مجدد رمز عبور خود در سایت {{ config('app.name') }} با ایمیل  {{ $email }} روی لینک زیر کلیک کنید.

@component('mail::button', ['url' => route('auth.admin.reset-password.form', ['token' => $token, 'email' => $email])] )
  تنظیم مجدد رمز عبور
@endcomponent

با تشکر از :<br>
{{ config('app.name') }}
@endcomponent
