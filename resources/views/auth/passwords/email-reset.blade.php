@extends('layouts.auth')

@section('content')
    <!--begin::Body-->
    <!--begin::Card-->
    <div class="card rounded-3 w-md-550px">
        <!--begin::Card body-->
        <div class="card-body p-10 p-lg-20">
            <!--begin::Form-->
            <form class="form w-100" action="{{ route('auth.admin.forget-password') }}" method="post">
                @csrf
                <!--begin::Heading-->
                <div class="text-center mb-10">
                    <!--begin::Title-->
                    <h3 class="text-dark fw-bolder mb-3">فراموشی رمز عبور ؟</h3>
                    <!--end::Title-->
                    <!--begin::Link-->
                    <div class="text-gray-500 fw-semibold fs-6">ایمیل مورد نظر را جهت تنظیم مجدد رمز عبور وارد کنید .</div>
                    <!--end::Link-->
                </div>
                <!--begin::Heading-->
                <!--begin::Alert-->
                @if (Session::get('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                @if (Session::get('fail'))
                    <div class="alert alert-danger">
                        {{ Session::get('fail') }}
                    </div>
                @endif
                <!--end::Alert-->
                <!--begin::Input group=-->
                <div class="fv-row mb-8">
                    <!--begin::Email-->
                    <input type="text" placeholder="ایمیل" name="email" autocomplete="off" class="form-control bg-transparent" />
                    <!--end::Email-->
                    @error('email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!--begin::Actions-->
                <div class="d-flex flex-wrap justify-content-center pb-lg-0">
                    <button type="submit" class="btn btn-primary me-4">
                        <!--begin::Indicator label-->
                        <span class="indicator-label">ارسال</span>
                        <!--end::Indicator label-->

                    </button>
                    <a href="{{route('auth.admin.login.form')}}" class="btn btn-light">لغو</a>
                </div>
                <!--end::Actions-->
            </form>
            <!--end::Form-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

    <!--end::Body-->
@endsection
