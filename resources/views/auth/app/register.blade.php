@extends('layouts.auth')

@section('content')

    <!--begin::Card-->
    <div class="card rounded-3 w-md-550px">
        <!--begin::Card body-->
        <div class="card-body p-10 p-lg-20">
            <!--begin::Form-->
            <form class="form w-100"  action="{{route('auth.app.register')}}" method="post">
                @csrf
                <!--begin::Heading-->
                <div class="text-center mb-11">
                    <!--begin::Title-->
                    <h1 class="text-dark fw-bolder mb-3">ورود به پنل کاربری</h1>
                    <!--end::Title-->
                </div>
                <!--begin::Heading-->
                <!--begin::Login options-->
                <div class="row g-3 mb-9">
                    <!--begin::Col-->
                    <div class="col-md-12">
                        <!--begin::Google link=-->
                        <a href="{{ route('auth.admin.google.redirect')}}" class="btn btn-outline btn-text-gray-700 btn-active-color-primary w-100">
                        <img alt="Logo" src="{{asset('admin-assets/media/svg/brand-logos/google-icon.svg')}}" class="h-15px me-3" />ورود با اکانت گوگل</a>
                        <!--end::Google link=-->
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Login options-->
                <!--begin::Separator-->
                <div class="separator separator-content my-14">
                    <span class="w-125px text-gray-500 fw-semibold fs-7">ورود با ایمیل</span>
                </div>
                <!--end::Separator-->
                <!--begin::Input group=-->
                <div class="fv-row mb-8">
                    <!--begin::Email-->
                    <input type="text" placeholder="نام" name="name" autocomplete="off" class="form-control bg-transparent" id="name" />
                    <!--end::Email-->
                    @error('name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!--end::Input group=-->
                <!--begin::Input group=-->
                <div class="fv-row mb-8">
                    <!--begin::Email-->
                    <input type="text" placeholder="ایمیل" name="email" autocomplete="off" class="form-control" id="email" />
                    <!--end::Email-->
                    @error('email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!--end::Input group=-->
                <div class="fv-row mb-3">
                    <!--begin::Password-->
                    <input type="password" placeholder="رمز عبور" name="password" autocomplete="off" class="form-control bg-transparent" id="password" />
                    <!--end::Password-->
                    @error('password')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!--end::Input group=-->
                <!--begin::Wrapper-->
                <div class="d-flex flex-wrap gap-3 fs-base fw-semibold mb-8">
                    <div></div>
                    <!--begin::Link-->
                    <a href="#" class="link-primary text-right me-0">فراموشی رمز عبور</a>
                    <!--end::Link-->
                </div>
                <!--end::Wrapper-->
                <!--begin::Submit button-->
                <div class="d-grid mb-10">
                    <button type="submit" class="btn btn-primary">ورود</button>
                </div>
                <!--end::Submit button-->

            </form>
            <!--end::Form-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

@endsection
