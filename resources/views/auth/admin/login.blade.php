@extends('layouts.auth')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/css/sweetalert2.min.css')}}">
@endsection

@section('content')

    <!--begin::Card-->
    <div class="card rounded-3 w-md-550px">
        <!--begin::Card body-->
        <div class="card-body p-10 p-lg-20">
            <!--begin::Form-->
            <form class="form w-100"  action="{{route('auth.admin.login')}}" method="post">
                @csrf
                <!--begin::Heading-->
                <div class="text-center mb-11">
                    <!--begin::Title-->
                    <h1 class="text-dark fw-bolder mb-3">ورود به پنل مدیریت</h1>
                    <!--end::Title-->
                </div>
                <!--begin::Heading-->
                <!--begin::Login options-->
                <div class="row g-3 mb-9">
                    <!--begin::Col-->
                    <div class="col-md-12">
                        <!--begin::Google link=-->
                        <a href="{{ route('auth.admin.google.redirect') }}" class="btn btn-outline btn-text-gray-700 btn-active-color-primary w-100">
                        <img alt="Logo" src="{{asset('admin-assets/media/svg/brand-logos/google-icon.svg')}}" class="h-15px me-3" />ورود با اکانت گوگل</a>
                        <!--end::Google link=-->
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Login options-->
                <!--begin::Separator-->
                <div class="separator separator-content my-14">
                    <span class="w-125px text-gray-500 fw-semibold fs-7">ورود با ایمیل</span>
                </div>
                <!--end::Separator-->
                <!--begin::Error-->
                @if ($errors->count() > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <ul>
                                <li>{{ $error }}</li>
                            </ul>
                        @endforeach
                    </div>
                @endif
                <!--end::Error-->
                <!--begin::Alert-->
                @if (Session::get('info'))
                    <div class="alert alert-info">
                        {{ Session::get('info') }}
                    </div>
                @endif
                <!--end::Alert-->
                <!--begin::Input group=-->
                <div class="fv-row mb-8">
                    <!--begin::Email-->
                    <input type="text" placeholder="ایمیل" name="email" autocomplete="true" class="form-control bg-transparent" id="email" />
                    <!--end::Email-->
                    @error('email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!--end::Input group=-->
                <div class="fv-row mb-3">
                    <!--begin::Password-->
                    <input type="password" placeholder="رمز عبور" name="password" autocomplete="off" class="form-control bg-transparent" id="password" />
                    <!--end::Password-->
                    @error('password')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!--end::Input group=-->
                <!--begin::Wrapper-->
                <div class="d-flex flex-wrap gap-3 fs-base fw-semibold mb-8">
                    <div></div>
                    <!--begin::Link-->
                    <a href="{{url('auth/admin/forget-password')}}" class="link-primary text-right me-0">فراموشی رمز عبور</a>
                    <!--end::Link-->
                </div>
                <!--end::Wrapper-->
                <!--begin::Submit button-->
                <div class="d-grid mb-10">
                    <button type="submit" class="btn btn-primary">
                        <!--begin::Indicator label-->
                        <span class="indicator-label">ورود</span>
                        <!--end::Indicator label-->
                        <!--begin::Indicator progress-->
                        <span class="indicator-progress"> لطفا منتظر بمانید...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        <!--end::Indicator progress-->
                    </button>
                </div>
                <!--end::Submit button-->

            </form>
            <!--end::Form-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

@endsection

@section('script')
    <script src="{{asset('admin-assets/js/sweetalert2.min.js')}}"></script>
    <script>
        @if(session('success'))
        Swal.fire({
            icon: 'success',
            title: 'عملیات موفق',
            text: "{{session('success')}}",  // session('success') = with('success', '...')
            confirmButtonText: 'باشه'
        })
        @endif

        @if(session('warning'))
        Swal.fire({
            icon: 'warning',
            title: 'عملیات ناموفق',
            text: "{{session('warning')}}",  // session('success') = with('success', '...')
            confirmButtonText: 'باشه'
        })
        @endif
    </script>
@endsection
