@component('mail::message')
# ایمیل فعالسازی

The body of your message.

@component('mail::button', ['url' => route('active.user', $code)] )
فعال سازی اکانت
@endcomponent

با تشکر از :<br>
{{ config('app.name') }}
@endcomponent
