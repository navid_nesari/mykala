@extends('layouts.master')

@section('style')

@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'ثبت'])
@endsection

@section('content')
    <form action="{{ route('admin.roles.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-flush ms-3 me-2">
                    <div class="card-header">
                        <h3 class="card-title">ثبت مقام</h3>
                        <div class="card-toolbar">
                                <a href="{{route('admin.roles.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="name">نام مقام</label>
                                    <input type="text" class="form-control mt-1" name="name" id="name"
                                           placeholder=" نام مقام را وارد کنید" value="{{old('name')}}">
                                    @error('name')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="permissions">اجازه دسترسی</label>
                                    <select name="permissions[]" id="permissions" class="form-control mt-1" multiple>
                                        @foreach ($permissions as $permission)
                                            <option value="{{$permission->id}}">{{$permission->name}} - {{ $permission->label }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-8">
                                    <label for="label">توضیحات</label>
                                    <textarea rows="5" class="form-control mt-1" name="label" id="label">
                                        {{old('label')}}
                                    </textarea>
                                    @error('label')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5 ms-3">
            <button type="submit" class="btn btn-primary">
                <span class="indicator-label">ثبت</span>
            </button>
        </div>
    </form>
@endsection

@section('script')
    <script>
        $('#permissions').select2({
            placeholder: 'مقادیر مد نظر را انتخاب کنید',
            // multiple: true
        });
    </script>
@endsection
