@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/added/sweetalert/sweetalert.min.css') }}" />
@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'مقالات'])
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card ms-3 me-3">
                <div class="app-container container-xxl d-flex flex-stack py-3">
                    <!--begin::Page title-->
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <!--begin::Title-->
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-4">
                            فهرست
                        </h1>
                        <span class="text-muted text-sm text-secondary" style="font-size: 10px;font-weight: bold">
                        تعداد کل :
                            <i class="badge badge-sm badge-light-warning px-2"
                               style="font-size: 10px">{{ $comments->count() }}</i>
                        </span>
                    </div>

                </div>
                <div class="card-body py-4">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        <table class="table align-middle table-row-dashed fs-6 gy-5 no-footer" id="kt_table_users">
                            <!--begin::Table head-->
                            <thead>
                            <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                <th class="min-w-125px">نام کاربر</th>
                                <th class="min-w-125px">متن کامنت</th>
                                <th class="min-w-125px">پست مربوطه</th>
                                <th class="min-w-125px">تنظیمات</th>
                            </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-semibold">
                            @foreach($comments as $comment)
                                <tr>
                                    {{-- @can('view', $comment) --}}
                                    <td>{{ $comment->user->name }}</td>
                                    <td>{{ Str::limit($comment->comment, 50) }}</td>
                                    <td><a href="{{ $comment->commentable->path() }}">{{ $comment->commentable->title }}</a></td>
                                    <td>

                                        <div class="d-flex">
                                            <a href="javascript:;"
                                            data-comment-id="{{$comment->id}}"
                                            class="btn btn-icon btn-sm btn-clean btn-light-danger btn-active-danger delete_comment"
                                            data-inbox="dismiss" data-toggle="tooltip" title="حذف">
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
                                                <span class="svg-icon svg-icon-2 p-1">
                                                    <i class="fa fa-trash"></i>
                                                </span>
                                                <!--end::Svg Icon-->
                                            </a>

                                            <form action="{{ route('admin.comments.update', $comment) }}" method="post">
                                                @csrf
                                                <button type="submit" class="btn btn-sm btn-light-success ms-1">تایید</button>
                                            </form>
                                        </div>
                                    </td>
                                    <!--end::Action=-->

                                    {{-- @endcan --}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end::Table-->
                    {{-- <div style="text-align: center">
                        {!! $comments->render() !!}
                    </div> --}}
                </div>

            </div>
        </div>

    </div>
@endsection

@section('script')
    <script src="{{ asset('admin-assets/plugins/added/sweetalert/sweetalert2.min.js') }}" ></script>

    <script type="text/javascript">
        $('.delete_comment').click(function (e) {
            e.preventDefault();

            // console.log('delete blog click !');
            console.log('comment id : ' + $(this).data('comment-id'));

            let comment_id = $(this).data('comment-id');

            let url = "{{ route('admin.comments.delete', ':comment-id')}}";
            console.log(url);
            url = url.replace(':comment-id', comment_id)
            console.log(url);

            Swal.fire({
                title: 'مطمئن هستید که می خواهید حذف کنید؟',
                text: "در صورت حذف فایل قابل بازگشت نمی باشد.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'بله. حذف شود'
            }).then((willDelete) => {
                if (willDelete.isConfirmed) {
                    window.location = url;
                    Swal.fire(
                        'حذف شد',
                        'فرد مورد نظر با موفقیت حذف شد.',
                        'success'
                    )
                }
            })
        })

        // set session to message(sweetalert) is shown `only once` -> flash session
        @if(session('success'))
        Swal.fire({
            icon: 'success',
            title: 'عملیات موفق',
            text: "{{session('success')}}",  // session('success') = with('success', '...')
            confirmButtonText: 'باشه'
        })
        @endif
    </script>
@endsection
