@extends('layouts.master')

@section('style')

@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'ثبت ویدیو'])
@endsection

@section('content')
    <form action="{{ route('admin.episodes.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-flush ms-3 me-2">
                    <div class="card-header">
                        <h3 class="card-title">ثبت ویدیو</h3>
                        <div class="card-toolbar">
                                <a href="{{route('admin.episodes.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group mb-8">
                                    <label for="title">عنوان ویدیو</label>
                                    <input type="text" class="form-control mt-1" name="title" id="title"
                                           placeholder="عنوان ویدیو را وارد کنید" value="{{old('title')}}">
                                    @error('title')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group mb-8">
                                    <label for="course_id">دوره مرتبط</label>
                                    <select name="course_id" id="course_id" class="form-control mt-1">
                                        @foreach ($courses as $course)
                                            <option value="{{$course['id']}}">{{$course['title']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group mb-8">
                                    <label for="type">نوع ویدیو</label>
                                    <select name="type" id="type" class="form-control mt-1">
                                        <option value="free" selected>رایگان</option>
                                        <option value="vip">اعضای ویژه</option>
                                        <option value="cache">نقدی</option>
                                    </select>
                                    @error('type')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="time" class="control-label">زمان ویدیو</label>
                                    <input type="text" class="form-control mt-1" name="time" id="time"
                                        placeholder="زمان را وارد کنید" value="{{ old('time') }}">
                                    @error('time')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="part_number" class="control-label">شماره قسمت</label>
                                    <input type="number" class="form-control mt-1" name="part_number" id="part_number"
                                        placeholder="شماره قسمت را وارد کنید" value="{{ old('part_number') }}">
                                    @error('part_number')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="video_url" class="control-label">لینک ویدیو</label>
                                    <input type="text" class="form-control mt-1" name="video_url" id="video_url"
                                        placeholder="لینک را وارد کنید" value="{{ old('video_url') }}">
                                    @error('video_url')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="tags" class="control-label">تگ ها</label>
                                    <input type="text" class="form-control mt-1" name="tags" id="tags"
                                        placeholder="تگ ها را وارد کنید" value="{{ old('tags') }}">
                                    @error('tags')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-8">
                                    <label for="description">متن</label>
                                    <textarea rows="5" class="form-control mt-1" name="description" id="description">{{old('description')}}</textarea>
                                    @error('description')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5 ms-3">
            <button type="submit" class="btn btn-primary">
                <span class="indicator-label">ثبت</span>
            </button>
        </div>
    </form>
@endsection

@section('script')
    <script src="{{asset('ckeditor5/ckeditor.js')}}"></script>
    <script>
       ClassicEditor.create( document.querySelector( '#description' ), {
        ckfinder: {
            uploadUrl: "{{route('admin.articles.uploadImageWithCkeditor') . '?_token=' . csrf_token()}}",

        },
        language: 'fa',
        title: false,
        toolbar: {
                    items: [
                        'imageUpload',  'imageTextAlternative', 'mediaEmbed', '|',
                        'blockQuote','|', 'bold', 'italic',
                        'bulletedList', 'numberedList', 'link', '|', 'heading', '|',
                        'undo', 'redo', 'selectAll', '|',
                        'Indent', 'Outdent', '|',
                        'insertTable', 'tableColumn', 'tableRow', 'mergeTableCells', '|',
                    ],
                    viewportTopOffset: 30,
                    shouldNotGroupWhenFull: true,
                },
    } )
        .then(editor => {
            console.log(editor);
        })
        .catch( error => {
            console.error( error );
        });
    </script>
@endsection
