@extends('layouts.master')

@section('style')

@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'ثبت دوره'])
@endsection

@section('content')
    <form action="{{ route('admin.courses.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-flush ms-3 me-2">
                    <div class="card-header">
                        <h3 class="card-title">ثبت دوره</h3>
                        <div class="card-toolbar">
                                <a href="{{route('admin.courses.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group mb-8">
                                    <label for="title">عنوان دوره</label>
                                    <input type="text" class="form-control mt-1" name="title" id="title"
                                           placeholder="عنوان دوره را وارد کنید" value="{{old('title')}}">
                                    @error('title')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group mb-8">
                                    <label for="tags">تگ</label>
                                    <input type="text" class="form-control mt-1" name="tags" id="tags"
                                           placeholder="تگ را وارد کنید" value="{{old('tags')}}">
                                    @error('tags')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group mb-8">
                                    <label for="image">عکس مقاله</label>
                                    <input type="file" class="form-control mt-1" name="image" id="image"
                                           placeholder="عکس مقاله را وارد کنید" value="{{old('images')}}">
                                    @error('images')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="type">نوع دوره</label>
                                    <select name="type" id="type" class="form-control mt-1">
                                        <option value="free" selected>رایگان</option>
                                        <option value="vip">اعضای ویژه</option>
                                        <option value="cache">نقدی</option>
                                    </select>
                                    @error('type')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="price">قیمت دوره</label>
                                    <input type="text" class="form-control mt-1" name="price" id="price"
                                        placeholder="قیمت" value="{{old('price')}}">
                                    @error('price')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-8">
                            <label for="body">متن</label>
                            <textarea rows="5" class="form-control mt-1" name="body" id="body">{{old('body')}}</textarea>
                            @error('body')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5 ms-3">
            <button type="submit" class="btn btn-primary">
                <span class="indicator-label">ثبت</span>
            </button>
        </div>
    </form>
@endsection

@section('script')
    <script src="{{asset('ckeditor5/ckeditor.js')}}"></script>
    <script>
       ClassicEditor.create( document.querySelector( '#body' ), {
        ckfinder: {
            uploadUrl: "{{route('admin.articles.uploadImageWithCkeditor') . '?_token=' . csrf_token()}}",

        },
        language: 'fa',
        title: false,
        toolbar: {
                    items: [
                        'imageUpload',  'imageTextAlternative', 'mediaEmbed', '|',
                        'blockQuote','|', 'bold', 'italic',
                        'bulletedList', 'numberedList', 'link', '|', 'heading', '|',
                        'undo', 'redo', 'selectAll', '|',
                        'Indent', 'Outdent', '|',
                        'insertTable', 'tableColumn', 'tableRow', 'mergeTableCells', '|',
                    ],
                    viewportTopOffset: 30,
                    shouldNotGroupWhenFull: true,
                },
    } )
        .then(editor => {
            console.log(editor);
        })
        .catch( error => {
            console.error( error );
        });
    </script>
@endsection
