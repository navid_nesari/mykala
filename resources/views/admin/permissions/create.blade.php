@extends('layouts.master')

@section('style')

@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'ثبت', 'subTitle' => 'مجوز جدید'])
@endsection

@section('content')
    <form action="{{ route('admin.permissions.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-flush ms-3 me-2">
                    <div class="card-header">
                        <h3 class="card-title">ثبت مجوز</h3>
                        <div class="card-toolbar">
                                <a href="{{route('admin.permissions.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-8">
                                    <label for="name">نام مجوز</label>
                                    <input type="text" class="form-control mt-1" name="name" id="name"
                                           placeholder=" نام مجوز را وارد کنید" value="{{old('name')}}">
                                    @error('name')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-8">
                                    <label for="label">توضیحات</label>
                                    <textarea rows="5" class="form-control mt-1" name="label" id="label">
                                        {{old('label')}}
                                    </textarea>
                                    @error('label')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5 ms-3">
            <button type="submit" class="btn btn-primary">
                <span class="indicator-label">ثبت</span>
            </button>
        </div>
    </form>
@endsection

@section('script')
    <script>
        $('#permissions').select2({
            placeholder: 'مقادیر مد نظر را انتخاب کنید',
            // multiple: true
        });
    </script>
@endsection
