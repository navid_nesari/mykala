@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/css/sweetalert2.min.css')}}">
@endsection

@section('content')
    @include('admin.partials.toolbar')
@endsection

@section('script')
    <script src="{{asset('admin-assets/js/sweetalert2.min.js')}}"></script>
    <script>
        @if(session('success'))
        Swal.fire({
            icon: 'success',
            title: 'عملیات موفق',
            text: "{{session('success')}}",  // session('success') = with('success', '...')
            confirmButtonText: 'باشه'
        })
        @endif

        @if(session('warning'))
        Swal.fire({
            icon: 'warning',
            title: 'عملیات موفق',
            text: "{{session('warning')}}",  // session('success') = with('success', '...')
            confirmButtonText: 'باشه'
        })
        @endif
    </script>
@endsection
