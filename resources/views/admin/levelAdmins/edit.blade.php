@extends('layouts.master')

@section('style')

@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'ویرایش'])
@endsection

@section('content')
    <form action="{{ route('admin.levels.update', $user) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-flush ms-3 me-2">
                    <div class="card-header">
                        <h3 class="card-title">ویرایش مقام مدیر</h3>
                        <div class="card-toolbar">
                                <a href="{{route('admin.levels.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="form-group mb-8">
                                <label for="role_id"> مقام - {{$user->email}}</label>
                                <select name="role_id" id="role_id" class="form-control mt-1">
                                    @foreach ($roles as $role)
                                        <option value="{{$role->id}}" {{$user->hasRole($role->name) ? 'selected' :''}}  >
                                                {{$role->name}} - {{ $role->label }}
                                        </option>
                                        {{-- <option value="{{$role->id}}" @if (in_array($role->id, $user->roles->pluck('id')->toArray())) selected @endif >
                                            {{$role->name}} - {{ $role->label }}
                                        </option> --}}
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5 ms-3">
            <button type="submit" class="btn btn-primary">
                <span class="indicator-label">ثبت</span>
            </button>
        </div>
    </form>
@endsection

@section('script')
<script>
    $('#role_id').select2({
        placeholder: 'مقادیر مد نظر را انتخاب کنید',
        // multiple: true
    });
</script>
@endsection
