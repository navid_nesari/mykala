@extends('layouts.master')

@section('style')

@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'ثبت'])
@endsection

@section('content')
    <form action="{{ route('admin.levels.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-flush ms-3 me-2">
                    <div class="card-header">
                        <h3 class="card-title">ثبت مقام</h3>
                        <div class="card-toolbar">
                                <a href="{{route('admin.roles.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-8">
                                    <label for="user_id"> مدیران</label>
                                    <select name="user_id" id="user_id" class="form-control mt-1" >
                                        @foreach ($users as $user)
                                            <option value="{{$user->id}}">{{ $user->email }} - {{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('user_id')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-8">
                                    <label for="role_id">مقام ها </label>
                                    <select name="role_id" id="role_id" class="form-control mt-1">
                                        @foreach ($roles as $role)
                                            <option value="{{$role->id}}">{{$role->name}} - {{ $role->label }}</option>
                                        @endforeach
                                    </select>
                                    @error('role_id')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5 ms-3">
            <button type="submit" class="btn btn-primary">
                <span class="indicator-label">ثبت</span>
            </button>
        </div>
    </form>
@endsection

@section('script')
    <script>


        $('#role_id').select2({
            placeholder: 'مقادیر مد نظر را انتخاب کنید',
            // multiple: true,
            // matcher: matchStart
        });

        $('#user_id').select2({
            placeholder: 'مقادیر مد نظر را انتخاب کنید',
            // multiple: true,
            // matcher: matchStart
        });
    </script>
@endsection
