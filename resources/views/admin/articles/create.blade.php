@extends('layouts.master')

@section('style')

@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'ثبت مقاله'])
@endsection

@section('content')
    <form action="{{ route('admin.articles.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-flush ms-3 me-2">
                    <div class="card-header">
                        <h3 class="card-title">ثبت مقاله</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group mb-8">
                                    <label for="title">عنوان مقاله</label>
                                    <input type="text" class="form-control mt-1" name="title" id="title"
                                           placeholder="عنوان مقاله را وارد کنید" value="{{old('title')}}">
                                    @error('title')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group mb-8">
                                    <label for="tags">تگ</label>
                                    <input type="text" class="form-control mt-1" name="tags" id="tags"
                                           placeholder="تگ را وارد کنید" value="{{old('tags')}}">
                                    @error('tags')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group mb-8">
                                    <label for="image">عکس مقاله</label>
                                    <input type="file" class="form-control mt-1" name="images" id="images"
                                           placeholder="عکس مقاله را وارد کنید" value="{{old('images')}}">
                                    @error('images')
                                    <p class="text-danger">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-8">
                            <label for="description">توضیحات کوتاه</label>
                            <textarea rows="5" class="form-control mt-1" name="description" id="description">{{old('description')}}</textarea>
                            @error('description')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="form-group mb-8">
                            <label for="body">متن</label>
                            <textarea rows="5" class="form-control mt-1" name="body" id="body">{{old('body')}}</textarea>
                            @error('body')
                            <p class="text-danger">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5 ms-3">
            <button type="submit" class="btn btn-primary">
                <span class="indicator-label">ثبت</span>
            </button>
        </div>
    </form>
@endsection

@section('script')
    <script src="{{asset('ckeditor5/ckeditor.js')}}"></script>
    <script>
       ClassicEditor.create( document.querySelector( '#body' ), {
        ckfinder: {
            uploadUrl: "{{route('admin.articles.uploadImageWithCkeditor') . '?_token=' . csrf_token()}}",

        },
        language: 'fa',
        title: false,
        toolbar: {
                    items: [
                        'imageUpload',  'imageTextAlternative', 'mediaEmbed', '|',
                        'blockQuote','|', 'bold', 'italic',
                        'bulletedList', 'numberedList', 'link', '|', 'heading', '|',
                        'undo', 'redo', 'selectAll', '|',
                        'Indent', 'Outdent', '|',
                        'insertTable', 'tableColumn', 'tableRow', 'mergeTableCells', '|',
                    ],
                    viewportTopOffset: 30,
                    shouldNotGroupWhenFull: true,
                },
    } )
        .then(editor => {
            console.log(editor);
        })
        .catch( error => {
            console.error( error );
        });
    </script>
@endsection
