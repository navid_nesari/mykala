@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('admin-assets/plugins/custom/jstree/jstree.bundle.rtl.css')}}">
@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'ویرایش مقاله'])
@endsection

@section('content')
    <div id="kt_content_container" class="container-xxl">
        <div class="row mt-4">
            <form class="form d-flex flex-column flex-lg-row mb-3"
                  action="{{ route('admin.articles.update', $article) }}" method="post"
                  enctype="multipart/form-data">
            @csrf
            <!--begin::Main column-->
                <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10 ms-lg-2" style="margin-left: 20px ">
                    <!--begin::General options-->
                    <div class="card card-flush py-4">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <div class="card-title">
                                <h3>ویرایش مقاله</h3>
                            </div>
                            <div class="card-toolbar">
                                <a href="{{route('admin.articles.all')}}" class="btn btn-sm btn-light-success "
                                   style="margin-left: 5px">
                                    بازگشت
                                </a>

                            </div>
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 mt-2">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <div class="form-group mb-8">
                                            <!--start::Label-->
                                            <label for="title">عنوان مقاله</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control mt-1" name="title" id="title"
                                                placeholder="عنوان مقاله را وارد کنید" value="{{old('title', $article->title)}}">
                                            @error('title')
                                            <p class="text-danger">{{$message}}</p>
                                            @enderror
                                        </div>
                                        <!--end::Input-->
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <div class="form-group mb-8">
                                            <!--begin::Label-->
                                            <label for="tags">تگ</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control mt-1" name="tags" id="tags"
                                                placeholder="تگ را وارد کنید" value="{{old('tags', $article->tags)}}">
                                            @error('tags')
                                            <p class="text-danger">{{$message}}</p>
                                            @enderror
                                            <!--end::Input-->
                                        </div>
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <div class="form-group mb-8">
                                            <!--begin::Label-->
                                            <label for="description">توضیحات کوتاه</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <textarea rows="5" class="form-control mt-1" name="description" id="description">
                                                {{ old('description', $article->description) }}
                                            </textarea>

                                            @error('description')
                                            <p class="text-danger">{{$message}}</p>
                                            @enderror
                                            <!--end::Input-->
                                        </div>
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <div class="form-group mb-8">
                                            <!--begin::Label-->
                                            <label for="body">متن</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <textarea rows="8" class="form-control mt-1" name="body" id="body">
                                                {{ old('body', $article->body) }}
                                            </textarea>

                                            @error('body')
                                            <p class="text-danger">{{$message}}</p>
                                            @enderror
                                            <!--end::Input-->
                                        </div>
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <div class="form-group mb-8">
                                            <!--begin::Label-->
                                            <label for="images">عکس مقاله</label>
                                            <!--end::Label-->
                                            {{-- @dd($article->images['thumb']) --}}
                                            <!--begin::Input-->
                                            <input type="file" class="form-control mt-1" name="images" id="images"
                                                placeholder="عکس مقاله را وارد کنید">
                                            @error('images')
                                            <p class="text-danger">{{$message}}</p>
                                            @enderror
                                            <!--end::Input-->
                                        </div>
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-10 fv-row fv-plugins-icon-container">
                                        <div class="form-group mb-8">
                                            <!--begin::Label-->
                                            @foreach ( $article->images['imagesAddress'] as $key => $image)
                                            <label for="imagesThumb">
                                                {{$key}}

                                                <input type="radio" class=" mt-1 mb-1" name="imagesThumb" id="imagesThumb"
                                                    value="{{$image}}" @if ($article->images['thumb'] == $image) checked="checked" @endif >

                                                <img src="{{asset($image)}}" class="w-125px h-125px me-2">
                                            </label>

                                            @endforeach
                                            <!--end::Label-->
                                            <!--begin::Input-->

                                            @error('imagesThumb')
                                            <p class="text-danger">{{$message}}</p>
                                            @enderror
                                            <!--end::Input-->
                                        </div>
                                        <div class="fv-plugins-message-container invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card header-->
                    </div>
                    <!--end::General options-->
                    <div class="d-flex justify-content-start ms-1">
                        <!--begin::Button-->
                        <button type="submit" class="btn btn-primary">
                            <span class="indicator-label">ثبت تغییرات</span>
                        </button>
                        <!--end::Button-->
                    </div>
                </div>
                <!--end::Main column-->
            </form>
        </div>
    </div>
@endsection
@section('script')
<script src="{{asset('ckeditor5/ckeditor.js')}}"></script>
    <script>
       ClassicEditor.create( document.querySelector( '#body' ), {
        ckfinder: {
            uploadUrl: "{{route('admin.articles.uploadImageWithCkeditor') . '?_token=' . csrf_token()}}",

        },
        language: 'fa',
        title: false,
        toolbar: {
                    items: [
                        'imageUpload',  'imageTextAlternative', 'mediaEmbed', '|',
                        'blockQuote','|', 'bold', 'italic',
                        'bulletedList', 'numberedList', 'link', '|', 'heading', '|',
                        'undo', 'redo', 'selectAll', '|',
                        'Indent', 'Outdent', '|',
                        'insertTable', 'tableColumn', 'tableRow', 'mergeTableCells', '|',
                    ],
                    viewportTopOffset: 30,
                    shouldNotGroupWhenFull: true,
                },
    } )
        .then(editor => {
            console.log(editor);
        })
        .catch( error => {
            console.error( error );
        });
    </script>
@endsection
