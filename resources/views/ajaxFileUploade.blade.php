@extends('layouts.master')

@section('style')

@endsection

@section('toolbar')
    @include('admin.partials.toolbar', ['pageTitle' => 'دانلود فایل به صورت ای جکس'])
@endsection

@section('content')
    <form action="{{ route('ajax.file.upload') }}" method="POST" enctype="multipart/form-data" id="form">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="card card-flush ms-3 me-2">
                    <div class="card-header">
                        <h3 class="card-title">دانلود فایل</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="fileUploaded">فایل مربوطه</label>
                                    <input type="file" class="form-control mt-1" name="fileUploaded" id="fileUploaded">
                                    @error('fileUploaded')
                                    <p class="text-danger error-text">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group mb-8">
                                    <label for="title">عنوان</label>
                                    <input type="text" class="form-control mt-1" name="title" id="title">
                                    @error('title')
                                    <p class="text-danger error-text">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5 ms-3">
            <button type="submit" class="btn btn-primary">
                <span class="indicator-label">ثبت</span>
            </button>
        </div>
    </form>
@endsection

@section('script')

<script type="text/javascript">

    $(document).ready(function() {
        $('#form').on('submit', function(event) {
            event.preventDefault()

            var fileUploaded = $('#fileUploaded')[0].files[0];
            var title = $('#title').val();
            var _token = $('input[name="_token"]').val();

            var formData = new FormData();
            formData.append('fileUploaded', fileUploaded);
            formData.append('title', title);

            $.ajax({
                type:'POST',
                url: "{{ url('/ajax-file-uploade') }}",
                data: formData,
                // cache:false,
                contentType: false,
                processData: false,
                headers:{
                    'X-CSRF-TOKEN': _token
                }
            }).done(function(msg) {
                console.log(msg);
            });
            console.log(title, fileUploaded, $('input[name="_token"]').val());


            // var fileUploaded = $('#fileUploaded')[0].files[0];
            // var title = $('#title').val();
            // var _token = $('input[name="_token"]').val();

            // var formData = new FormData();
            // formData.append('fileUploaded', fileUploaded);
            // formData.append('title', title);

            // $.ajax({
            //     type:'POST',
            //     url: "{{ url('/ajax-file-uploade') }}",
            //     data: formData,
            //     // cache:false,
            //     contentType: false,
            //     processData: false,
            //     headers:{
            //         'X-CSRF-TOKEN': _token
            //     },
            //     success: (response) => {
            //         if (response) {
            //             $('#form')[0].reset();
            //             alert('File has been uploaded successfully');
            //         }
            //     },

            //     // error: function(response){
            //     //     $('#title').text(response.responseJSON.message);
            //     // }
            // })


            // var form = this;
            // $.ajax({
            //     url:$(form).attr('action'),
            //     method:$(form).attr('method'),
            //     data:new FormData(form),
            //     contentType: false,
            //     processData: false,
            //     dataType:'json',
            //     beforeSend:function(){
            //         $(form).find('p.error-text').text('');
            //     },
            //     success:function(data){
            //         if (data.code == 0) {
            //             $.each(data.error, function(prefix, val){
            //                 $(form).find('p.'+prefix+'_error').text(val[0]);
            //                 // console.log(prefix, val);
            //             });
            //         }else{
            //             $(form)[0].reset();
            //             alert('فایل مورد نظر دره اپلود مره.')
            //         }
            //     }
            // })
        })

    })
</script>
@endsection
