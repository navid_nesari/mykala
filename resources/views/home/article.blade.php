@extends('layouts.app')


@section('content')

    <!-- Blog Post Content Column -->
    <div class="col-lg-8">

        <!-- Blog Post -->

        <!-- Title -->
        <h1>{{ $article->title }}</h1>

        <!-- Author -->
        <p class="lead">
            توسط <a href="#">{{ $article->user->name }}</a>
        </p>

        <hr>

        <!-- DateTime -->
        <p><span class="glyphicon glyphicon-time"></span> ارسال شده در {{ jdate($article->created_at)->ago() }}</p>

        <hr>
        <!-- Preview Image -->
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive center-block" src="{{asset($article->images['imagesAddress'][300])}}" alt="">
            </div>
        </div>
        <hr>

        <!-- Post Content -->
        <p>{{ $article->description }}</p>
        <hr>

        <!-- Blog Comments -->

        @include('home.comment', ['comments' => $comments, 'subject' => $article])

    </div>

    <!-- Blog Sidebar Widgets Column -->
    <div class="col-md-4">

        <!-- Blog Search Well -->
        <div class="well">
            <h4>جستجو در سایت</h4>
            <div class="input-group">
                <input type="text" class="form-control">
                <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                    </button>
                    </span>
            </div>
            <!-- /.input-group -->
        </div>

        <!-- Side Widget Well -->
        <div class="well">
            <h4>دیوار</h4>
            <p>طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد.</p>
        </div>

    </div>


@endsection

@section('script')
<script>
    $('#sendCommentModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var parentId = button.data('parent') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        
        modal.find('.modal-title').text('ارسال پاسخ به' + parentId)
        modal.find("[name='parent_id']").val(parentId)
    })


</script>
@endsection
