<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title(),
            'slug' =>  $this->faker->slug(3),
            'description' =>  $this->faker->text(10),
            'body' =>  $this->faker->text(20),
            'images' =>  $this->faker->imageUrl,
            'tags' =>  Str::random(4),
        ];
    }
}
