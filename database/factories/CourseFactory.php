<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CourseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title(),
            'slug' =>  $this->faker->slug(3),
            'type' =>  $this->faker->title(),
            'price' =>  $this->faker->numberBetween(100,500) . 'هزار تومان',
            'body' =>  $this->faker->text(20),
            'image' =>  $this->faker->imageUrl,
            'tags' =>  Str::random(4),
        ];
    }
}
