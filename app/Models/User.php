<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRole;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'level',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function articles()
    {
        return $this->hasMany(Article::class);
    }


    public function courses()
    {
        return $this->hasMany(Course::class);
    }


    public function activationcodes()
    {
        return $this->hasMany(ActivationCode::class);
    }


    public function isAdmin()
    {
        return $this->level == 'admin' ? true : false;
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function checkLearning($course)
    {
        return !! Learn::where('user_id', $this->id)->where('course_id', $course->id)->first();
    }
}
