<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Course extends Model
{
    use HasFactory, Sluggable;

    protected $hidden = [
        'view_count',
        'comment_count',
    ];

    protected $guarded = [
        'id'
    ];

    protected $casts = [
        'image' => 'array',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate' => true
            ]
        ];
    }

    public function setBodyAttribute($value)
    {
        $this->attributes['description'] = Str::limit(preg_replace('/<[^>]*>/' , '' , $value), 200);
        $this->attributes['body'] = $value;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function path()
    {
        return "/home/courses/$this->slug";
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
