<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use function PHPUnit\Framework\isEmpty;

class ActivationCode extends Model
{
    use HasFactory;

    protected $guarded = [
        'id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function scopeCreateCode($query, $user)
    {
        $activationCode = $query->create([
            'user_id' => $user->id,
            'code' => $this->code(),
            'expire' => Carbon::now()->addMinutes(15),
        ]);

        return $activationCode;
    }


    private function code()
    {
        do {
            $code = Str::random(40).time();
            $check_code = static::where('code', $code)->first();
            // dd(is_null($check_code));
        } while (! isEmpty($check_code));

        return $code;
    }
}
