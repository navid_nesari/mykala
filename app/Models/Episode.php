<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    use HasFactory;

    protected $guarded = [
        'id'
    ];


    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function path()
    {
        return "courses/{$this->course->slug}/episode/$this->part_number";
    }
}
