<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Comment extends Model
{
    use HasFactory;

    protected $guarded = [
        'id'
    ];

    public function commentable(): MorphTo
    {
        return $this->morphTo();
    }

    // public function comments()
    // {
    //     return $this->hasMany(Comment::class, 'parent_id' , 'id')->where('approved', 1)->latest();
    // }
    // or
    public function comments()
    {
        return $this->hasMany(Comment::class, 'parent_id' , 'id');
    }

    public function setCommentAttribute($value)
    {
        $this->attributes['comment'] = str_replace(PHP_EOL, "<br>", $value);
    }

    // public function getCommentAttribute($value)
    // {
    //     $this->attributes['comment'] = str_replace("<br>", PHP_EOL, $value);
    // }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
