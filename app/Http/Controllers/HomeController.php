<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function ajaxFileUploade()
    {
        return view('ajaxFileUploade');
    }


    public function ajaxImageUploadPost(Request $request)
    {
        $validateDatas = request()->validate([
            'fileUploaded' => ['required', 'mimes:png,jpg,jpeg,csv,txt,pdf', 'max:2048'],
            'title' => ['required'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
        ]);
        foreach($validateDatas as $validateData) {
            if(!$validateData  /** !$validateData->passes() */ ) {
                // return $validateData->errors()->all();
                // return 'fail';
                return response()->json(['code' => 0, 'error' => $validateData->errors()->toArray()]);
            }
        }
        $inputFile = $request->file('fileUploaded');
        $path = '/uploads/ajaxUploads/images/';
        $imageName = time() . '-' . $inputFile->getClientOriginalName();
        $file = $inputFile->move(public_path($path), $imageName);

        // return response()->json(['success' => true]);
        return response()->json(['code' => 1, 'msg' => 'فایل مورد نظر با موفقیت ذخیره شد.']);
    }
}
