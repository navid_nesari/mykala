<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        // return auth()->user();
        // auth()->logout();
        // auth()->user()->hasRole(Permission::where('name', 'show-comment')->first()->roles);

        return view('admin.dashboard');
    }
}
