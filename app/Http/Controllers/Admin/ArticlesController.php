<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Gate;


class ArticlesController extends BaseController
{
    public function all()
    {
        // auth()->loginUsingId(10);
        // $articles = Article::latest()->paginate();

        // foreach($articles as $article) {
        //     if(Gate::allows('view', $article)) {
        //         return view('admin.articles.all', compact('articles'));
        //     }
        // }
        // return abort(403, 'no access');


        $articles = Article::latest()->paginate();

        return view('admin.articles.all', compact('articles'));
    }


    public function create()
    {

        return view('admin.articles.create');
    }


    public function store(Request $request)
    {
        // auth()->loginUsingId(1);
        // validation
        \request()->validate([
           'title' => ['required', 'unique:articles', 'max:100'],
           'description' => ['required', 'min:3', 'max:300'],
           'body' => ['required', 'min:3', 'max:400'],
           'tags' => ['required', 'string'],
           'images' => ['required', 'mimes:jpg,bmp,png'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'title.unique' => 'عنوان مورد نظر موجود است.',
            'title.max' => 'فیلد مورد نظر بیش از صد کارکتر است.',
            'description.max' => 'فیلد مورد نظر بیش از سیصد کارکتر است.',
            'body.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
            '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
            'images.mimes' => 'فرمت عکس مورد نظر اشتباه است.',
        ]);

        // data
        $data = [
            'title' => request('title'),
            'description' => request('description'),
            'body' => request('body'),
            'tags' => request('tags'),
        ];

        $path = '/uploads/articles/images/';
        $data['images'] = $this->uploadImages($request->file('images'), $path);

        $user = Auth::user();
        $user->articles()->create($data);

        return redirect()->route('admin.articles.all');
    }


    public function edit(Article $article)
    {
        return view('admin.articles.edit', compact('article'));
    }


    public function update(Request $request, Article $article)
    {
        // validation
        \request()->validate([
            'title' => ['required', 'max:100', Rule::unique('articles', 'title')->ignore($article)],
            'description' => ['required', 'min:3', 'max:300'],
            'body' => ['required', 'min:3', 'max:400'],
            'tags' => ['required', 'string'],
            'images' => ['mimes:jpg,bmp,png'],
            'imagesThumb' => ['required']
         ],[
             '*.required' => 'فیلد مورد نظر الزامی است.',
             'title.unique' => 'عنوان مورد نظر موجود است.',
             'title.max' => 'فیلد مورد نظر بیش از صد کارکتر است.',
             'description.max' => 'فیلد مورد نظر بیش از سیصد کارکتر است.',
             'body.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
             '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
             'images.mimes' => 'فرمت عکس مورد نظر اشتباه است.',
         ]);

         // data
        $data = [
            'title' => request('title'),
            'description' => request('description'),
            'body' => request('body'),
            'tags' => request('tags'),
        ];

        if(request('images')) {
            $path = '/uploads/articles/images/';
            $data['images'] = $this->uploadImages($request->file('images'), $path);
        }else {
            $data['images'] = $article->images;
            $data['images']['thumb'] = request('imagesThumb');
        }

        $article->update($data);

        return redirect()->route('admin.articles.all');
    }


    public function delete (Article $article)
    {
        $article->delete();

        return redirect()->back();
    }


    public function uploadImageWithCkeditor(Request $request)
    {
        if($request->hasFile('upload')) {

            $file = $request->file('upload');
            $imageName = time() . '-' . $file->getClientOriginalName();
            $path = '/uploads/articles/images/';

            if(file_exists(public_path($path) . $imageName)) {
                $imageName = time() . '_' . $file->getClientOriginalName();
            }
            $file->move(public_path($path), $imageName);

            //get filename with extension
            // $filenamewithextension = $request->file('upload')->getClientOriginalName();
            //get filename without extension
            // $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            //get file extension
            // $extension = $request->file('upload')->getClientOriginalExtension();
            //filename to store
            // $filenametostore = $filename.'_'.time().'.'.$extension;

            $url = asset($path . $imageName);

            return response()->json(['imageName' => $imageName, 'uploaded' => true, 'url' => $url]);
        }
    }
}
