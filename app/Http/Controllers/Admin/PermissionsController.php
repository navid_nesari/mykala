<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Http\Request;

class PermissionsController extends Controller
{
    public function all()
    {
        auth()->loginUsingId(11);
        $permissions = Permission::latest()->paginate();

        return view('admin.permissions.all', compact('permissions'));
    }


    public function create()
    {
        return view('admin.permissions.create');
    }


    public function store(Request $request)
    {
        // validation
        request()->validate([
           'name' => ['required'],
           'label' => ['required', 'min:3', 'max:400'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'label.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
            '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
        ]);

        // data
        $data = [
            'name' => request('name'),
            'label' => request('label'),
        ];

        Permission::create($data);

        return redirect()->route('admin.permissions.all');
    }


    public function edit(Permission $permission)
    {
        return view('admin.permissions.edit', compact('permission'));
    }


    public function update(Request $request, Permission $permission)
    {
        // validation
        request()->validate([
           'name' => ['required'],
           'label' => ['required', 'min:3', 'max:400'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'label.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
            '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
        ]);

        // data
        $data = [
            'name' => request('name'),
            'label' => request('label'),
        ];

        $permission->update($data);

        return redirect()->route('admin.permissions.all');
    }


    public function delete(Permission $permission)
    {
        $permission->delete();

        return redirect()->back();
    }
}
