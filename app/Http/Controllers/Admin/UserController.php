<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ActivationCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\isEmpty;

class UserController extends Controller
{
    public function activeToken($token)
    {
        $activationCode = ActivationCode::where('code', $token)->first();

        if(! $activationCode) {
            dd('the activation code is not available.');
        }

        if($activationCode->expire < Carbon::now()) {
            dd('expire');
        }

        if($activationCode->used == true) {
            dd('used');
        }

        $activationCode->used = true;
        $activationCode->save();

        $activationCode->user()->update(['active' => true]);

        Auth::guard('web')->login($activationCode->user);

        return redirect()->route('admin.dashboard');
    }
}
