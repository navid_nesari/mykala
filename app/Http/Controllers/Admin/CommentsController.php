<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function all()
    {
        $comments = Comment::where('approved', 1)->latest()->paginate();
        return view('admin.comments.all', compact('comments'));
    }

    public function unsuccessful()
    {
        $comments = Comment::where('approved', 0)->latest()->paginate();
        return view('admin.comments.approving', compact('comments'));
    }

    public function update(Comment $comment)
    {
        $comment->update(['approved' => 1]);
        $comment->commentable->increment('comment_count');
        return redirect()->back();
    }

    public function delete(Comment $comment)
    {
        $comment->commentable->decrement('comment_count');
        $comment->delete();
        return redirect()->back();
    }
}
