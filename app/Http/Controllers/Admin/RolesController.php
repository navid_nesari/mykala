<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public function all()
    {
        $roles = Role::latest()->paginate();

        return view('admin.roles.all', compact('roles'));
    }


    public function create()
    {
        $permissions = Permission::get();

        return view('admin.roles.create', compact('permissions'));
    }


    public function store(Request $request)
    {
        // validation
        request()->validate([
           'name' => ['required'],
           'permissions' => ['required'],
           'label' => ['required', 'min:3', 'max:400'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'label.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
            '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
        ]);

        // data
        $data = [
            'name' => request('name'),
            'label' => request('label'),
        ];

        $role = Role::create($data);
        $role->permissions()->sync(request('permissions'));

        return redirect()->route('admin.roles.all');
    }


    public function edit(Role $role)
    {
        $permissions = Permission::get();

        return view('admin.roles.edit', compact('role', 'permissions'));
    }


    public function update(Request $request, Role $role)
    {
        // validation
        request()->validate([
           'name' => ['required'],
           'permissions' => ['required'],
           'label' => ['required', 'min:3', 'max:400'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'label.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
            '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
        ]);

        // data
        $data = [
            'name' => request('name'),
            'label' => request('label'),
        ];

        $role->update($data);
        $role->permissions()->sync(request('permissions'));

        return redirect()->route('admin.roles.all');
    }


    public function delete(Role $role)
    {
        $role->delete();

        return redirect()->back();
    }
}
