<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CoursesController extends BaseController
{
    public function all()
    {
        $courses = Course::latest()->paginate();

        return view('admin.courses.all', compact('courses'));
    }


    public function create()
    {

        return view('admin.courses.create');
    }


    public function store(Request $request)
    {
        // auth()->loginUsingId(2);
        // validation
        request()->validate([
           'title' => ['required', 'unique:courses', 'max:100'],
           'type' => ['required'],
           'price' => ['required'],
           'body' => ['required', 'min:3', 'max:400'],
           'tags' => ['required', 'string'],
           'image' => ['required', 'mimes:jpg,bmp,png'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'title.unique' => 'عنوان مورد نظر موجود است.',
            'title.max' => 'فیلد مورد نظر بیش از صد کارکتر است.',
            'body.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
            '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
            'image.mimes' => 'فرمت عکس مورد نظر اشتباه است.',
        ]);

        // data
        $data = [
            'title' => request('title'),
            'type' => request('type'),
            'price' => request('price'),
            'body' => request('body'),
            'tags' => request('tags'),
        ];

        $path = '/uploads/courses/images/';
        $data['image'] = $this->uploadImages($request->file('image'), $path);

        auth()->user()->courses()->create($data);

        return redirect()->route('admin.courses.all');
    }

    public function edit(Course $course)
    {
        return view('admin.courses.edit', compact('course'));
    }


    public function update(Request $request, Course $course)
    {
        // validation
        \request()->validate([
            'title' => ['required', 'max:100', Rule::unique('courses', 'title')->ignore($course)],
            'type' => ['required'],
            'price' => ['required'],
            'body' => ['required', 'min:3', 'max:400'],
            'tags' => ['required', 'string'],
            'image' => ['mimes:jpg,bmp,png'],
            'imagesThumb' => ['required']
         ],[
             '*.required' => 'فیلد مورد نظر الزامی است.',
             'title.unique' => 'عنوان مورد نظر موجود است.',
             'title.max' => 'فیلد مورد نظر بیش از صد کارکتر است.',
             'body.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
             '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
             'image.mimes' => 'فرمت عکس مورد نظر اشتباه است.',
         ]);

         // data
         $data = [
            'title' => request('title'),
            'type' => request('type'),
            'price' => request('price'),
            'body' => request('body'),
            'tags' => request('tags'),
        ];

        if(request('image')) {
            $path = '/uploads/courses/images/';
            $data['image'] = $this->uploadImages($request->file('image'), $path);
        }else {
            $data['image'] = $course->image;
            $data['image']['thumb'] = request('imagesThumb');
        }

        $course->update($data);

        return redirect()->route('admin.courses.all');
    }


    public function delete (Course $course)
    {
        $course->delete();

        return redirect()->back();
    }

}
