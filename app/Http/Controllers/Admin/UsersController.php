<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function all()
    {
        $users = User::latest()->paginate();

        return view('admin.users.all', compact('users'));
    }


    public function delete(User $user)
    {
        $this->authorize('show-users');
        $user->delete();
        return redirect()->back();
    }
}
