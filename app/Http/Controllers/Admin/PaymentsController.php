<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    public function all()
    {
        $payments = Payment::with('user')->where('payment', 1)->latest()->paginate();

        return view('admin.payments.all', compact('payments'));
    }


    public function unsuccessful()
    {
        $payments = Payment::where('payment', 0)->with('user')->latest()->paginate();

        return view('admin.payments.unsuccess', compact('payments'));
    }


    public function update(Payment $payment)
    {
        $payment->update([
            'payment' => 1
        ]);

        return back();
    }


    public function delete(Payment $payment)
    {
        $payment->delete();

        return back();
    }
}
