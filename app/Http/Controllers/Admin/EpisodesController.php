<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Episode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;

class EpisodesController extends BaseController
{
    public function all()
    {
        $episodes = Episode::latest()->paginate();

        return view('admin.episodes.all', compact('episodes'));
    }


    public function create()
    {
        $courses = Course::select('id', 'title')->get()->toArray();

        return view('admin.episodes.create', compact('courses'));
    }


    public function store(Request $request)
    {
        // validation
        request()->validate([
           'title' => ['required', 'max:100'],
           'type' => ['required'],
           'course_id' => ['required'],
           'time' => ['required'],
           'description' => ['required', 'min:3', 'max:400'],
           'tags' => ['required', 'string'],
           'part_number' => ['required'],
           'video_url' => ['required'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'title.unique' => 'عنوان مورد نظر موجود است.',
            'title.max' => 'فیلد مورد نظر بیش از صد کارکتر است.',
            'description.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
            '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
        ]);

        // data
        $data = [
            'title' => request('title'),
            'type' => request('type'),
            'course_id' => request('course_id'),
            'description' => request('description'),
            'tags' => request('tags'),
            'time' => request('time'),
            'part_number' => request('part_number'),
            'video_url' => request('video_url'),
        ];

        $episode = Episode::create($data);
        $this->setCourseTime($episode);

        return redirect()->route('admin.episodes.all');
    }


    public function edit(Episode $episode)
    {
        $courses = Course::select('id', 'title')->get()->toArray();

        return view('admin.episodes.edit', compact('episode', 'courses'));
    }


    public function update(Episode $episode)
    {
        // validation
        request()->validate([
            'title' => ['required', 'max:100'],
            'type' => ['required'],
            'course_id' => ['required'],
            'time' => ['required'],
            'description' => ['required', 'min:3', 'max:400'],
            'tags' => ['required', 'string'],
            'part_number' => ['required'],
            'video_url' => ['required'],
         ],[
             '*.required' => 'فیلد مورد نظر الزامی است.',
             'title.unique' => 'عنوان مورد نظر موجود است.',
             'title.max' => 'فیلد مورد نظر بیش از صد کارکتر است.',
             'description.max' => 'فیلد مورد نظر بیش از چهارصد کارکتر است.',
             '*.min' => 'فیلد مورد نظر کمتر از سه کارکتر است.',
         ]);

         // data
        $data = [
             'title' => request('title'),
             'type' => request('type'),
             'course_id' => request('course_id'),
             'description' => request('description'),
             'tags' => request('tags'),
             'time' => request('time'),
             'part_number' => request('part_number'),
             'video_url' => request('video_url'),
        ];

        $episode->update($data);
        $this->setCourseTime($episode);

        return redirect()->route('admin.episodes.all');
    }



    public function delete (Episode $episode)
    {
        $episode->delete();

        return redirect()->back();
    }


    protected function setCourseTime($episode)
    {
        $course = $episode->course;

        // The first solution
        /*
        $timestamp = Carbon::parse('00:00:00');
        $courseTime = strtotime($course->time) + strtotime($episode->time);
        $course->time = $timestamp->addSecond($courseTime)->format('H:i:s');
        */

        // The second solution
        $episodesTime = $course->episodes->pluck('time')->toArray();
        $course->time = $this->getCourseTime($episodesTime);

        $course->save();
        // or
        // $course->update();
    }

    protected function getCourseTime($episodesTime)
    {
        $timestamp = Carbon::parse('00:00:00');

        foreach($episodesTime as $time) {
            $t = strlen($time) == 5 ? strtotime('00:' . $time) : strtotime($time);
            $timestamp->addSecond($t);
        }

        return $timestamp->format('H:i:s');
    }
}
