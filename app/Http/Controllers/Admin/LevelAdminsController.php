<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class LevelAdminsController extends Controller
{
    public function all()
    {
        $roles = Role::with('users')->latest()->paginate();

        return view('admin.levelAdmins.all', compact('roles'));
    }


    public function create()
    {
        $users = User::where('level', 'admin')->get();
        $roles = Role::all();
        // dd($roles);
        return view('admin.levelAdmins.create', compact('users', 'roles'));
    }


    public function store(Request $request)
    {
        // validation
        request()->validate([
           'user_id' => ['required'],
           'role_id' => ['required'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
        ]);

        $user = User::find(request('user_id'));
        // $user->roles()->attach(request('roles'));
        $user->roles()->sync(request('role_id'));

        return redirect()->route('admin.levels.all');
    }


    public function edit(User $level)
    {
        $user = $level;
        $roles = Role::all();

        return view('admin.levelAdmins.edit', compact('user', 'roles'));
    }


    public function update(Request $request, User $level)
    {
        $user = $level;
        // validation
        request()->validate([
            'role_id' => ['required'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
        ]);

        $user->roles()->sync(request('role_id'));

        return redirect()->route('admin.levels.all');
    }


    public function delete(User $level)
    {
        $user = $level;
        $user->roles()->detach();

        return redirect()->back();
    }

    // public function delete(Admin $level, Role $role)
    // {
    //     $admin = $level;
    //     $admin->roles()->detach($role->id);
    //     return redirect()->back();
    // }
}
