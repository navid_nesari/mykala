<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\User;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiController;
use Exception;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    use ApiController;

    public function all(User $user=null)
    {
        $users = User::all();

        return ($user)
            ? $this->responseSuccess('عملیات دریافت اطلاعات با موفقیت انجام شد.', $user, 200)
            : $this->responseSuccess('عملیات دریافت اطلاعات با موفقیت انجام شد.', $users, 200);
    }


    public function search(Request $request, string $name)
    {
        /*
        |--------------------------------------------------------------------------------
        | چندین شرط را چک میکنیم این شروط به صورت & عمل میکنند where() وقتی در متد
        | و باید تماما وارد شده و صدق شوند وگرنه هیچ رکوردی بازگردانده نمیشود
        | level, email, name مثلا در پایین حتما باید رکوردی در دیتابیس با مشخصات
        | وجود داشته باشد تا دیتا برگشت داده شود.
        |--------------------------------------------------------------------------------
        */


        // $user = User::where('name', 'like', '%'.$name.'%')->get();
        try{
            $user =User::where([
                'level' => request('level'),
                'email' => request('email'),
                ['name', 'like', '%'.$name.'%']
            ])->get();
            return response()->json($user,200);
        }catch(\Throwable $error){
            return response()->json($error->getMessage(),400);
        }
    }


    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی میباشد.',
            'email.email' => 'فیلد مورد نظر باید ایمیل باشد.',
            'email.unique' => 'فیلد مورد نظر باید یکتا باشد.'
        ]);

        if( $validator->fails() ) {
            return $this->responseError($validator->errors()->toArray(), 400);
        }

        $data = [
            'name' => request('name'),
            'email' => request('email'),
            'password' => request('password'),
        ];
        $user = User::create($data);

        return $this->responseSuccess('عملیات ایجاد کاربر با موفقیت انجام شد.', $user, 201);
    }


    public function update(Request $request)
    {
        $user = User::find($request->id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        $result = $user->save();

        if($result) {
            return $this->responseSuccess('عملیات ویرایش کاربر با موفقیت انجام شد.', $user, 200);
        }
        return $this->responseError($user, 500);
    }


    public function delete(User $user)
    {
        $result = $user->delete();

        if($result) {
            return $this->responseSuccess('کاربر مورد نظر با موفقیت حذف شد.', $user, 200);
        }
        return $this->responseError($user, 500);
    }
}
