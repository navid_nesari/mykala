<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends BaseController
{
    use ApiController;

    public function store(Request $request)
    {
        /*
        $request->has('email');
        $request->has(['email', 'name', 'phone']);
        */
        // dd($request->title);

        $data = [
            'user_id' => $request->user_id,
            'title' => request('title'),
            'description' => request('description'),
            'body' => request('body'),
            'tags' => request('tags'),
        ];

        $file = $request->images;
        $path = '/uploads/api-images/articles/';
        if( $request->hasFile('images') ) {
            $data['images'] = $this->uploadImages($file, $path);
        }
        $article = Article::create($data);
        $msg = 'عملیات ایجاد مقاله با موفقیت انجام شد.';

        return $this->responseSuccess($msg, $article, 201);
    }
}
