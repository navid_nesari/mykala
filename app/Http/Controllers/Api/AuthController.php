<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use ApiController;

    public function register(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string'],
                'email' => ['required', 'email', 'unique:users,email'],
                'password' => ['required'],
            ],[
                '*.required' => 'فیلد مورد نظر الزامی میباشد.',
                'email.email' => 'فیلد مورد نظر باید ایمیل باشد.',
                'email.unique' => 'فیلد مورد نظر باید یکتا باشد.'
            ]);

            if($validator->fails()){
                return $this->responseError($validator->errors()->toArray(), 401);
            }

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
            $token = $user->createToken("api-token")->plainTextToken;

            return $this->responseSuccess('کاربر مورد نظر با موفقیت ثبت شد.', [$user, $token], 201);

        }catch(Exception $error){
            return $this->responseError($error->getMessage(), 500);
        }
    }


    public function login(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'email' => ['required', 'email', 'exists:users'],
                'password' => ['required'],
            ],[
                '*.required' => 'فیلد مورد نظر الزامی میباشد.',
                'email.email' => 'فیلد مورد نظر باید ایمیل باشد.',
                'email.exists' => 'فیلد مورد نظر باید موجود نمیباشد.'
            ]);

            if($validator->fails()){
                return $this->responseError($validator->errors()->toArray(), 401);
            }

            if( !auth()->attempt(request(['email', 'password'])) ) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'مشخصات وارد شده معتبر نمیباشد.',
                ], 401);
            }

            $user = User::where('email', $request->email)->first();
            $token = $user->createToken("api-token")->plainTextToken;

            return $this->responseSuccess('کاربر مورد نظر با موفقیت وارد شد.', [$user, $token], 201);

        }catch(Exception $error){
            return response()->json($error->getMessage(), 500);
        }
    }
}
