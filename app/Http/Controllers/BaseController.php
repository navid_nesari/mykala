<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Intervention\Image\Facades\Image;

class BaseController extends Controller
{
    public function uploadImages(UploadedFile $file, $path)
    {

        $imageName = time() . '-' . $file->getClientOriginalName();
        
        $file = $file->move(public_path($path), $imageName);

        $sizes = ['300', '600', '900'];
        $url['imagesAddress'] = $this->resize($file, $path, $imageName, $sizes);
        $url['thumb'] = $url['imagesAddress'][$sizes[0]];

        return $url;
    }

    private function resize($file, $path, $imageName, $sizes)
    {
        $imagesAddress['orginal'] = $path . $imageName;

        foreach($sizes as $size) {
            $imagesAddress[$size] = $path . "{$size}_" . $imageName;

            Image::make($file->getRealPath())->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path($path . "{$size}_" . $imageName));
        }
        return $imagesAddress;
    }
}
