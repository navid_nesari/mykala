<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

trait ApiController
{
    public function responseSuccess($msg=null, $data, $code)
    {
        return response()->json([
            'status' => 'success',
            'message' => $msg,
            'data' => $data
        ], $code);
    }


    public function responseError($data, $code)
    {
        return response()->json([
            'status' => 'error',
            'message' => 'عملیات مورد نظر انجام نشد.',
            'data' => $data
        ], $code);
    }
}
