<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class ArticlesController extends Controller
{
    public function single(Article $article)
    {
        // Redis::incr("view_count.{$article->id}.articles");
        $article->increment('view_count');
        $comments = $article->comments()->where([['approved', 1], ['parent_id', 0]])->latest()->with(['comments' => function($query){
            $query->where('approved', 1)->latest();
        }])->get();

        return view('home.article', compact('article', 'comments'));
    }
}
