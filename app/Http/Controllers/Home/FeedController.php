<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class FeedController extends Controller
{
    public function articles()
    {
        $feed = app()->make('feed');
        $feed->setCache(1, 'laravel.feed.article');

        if(! $feed->isCached() ) {
            $articles = Article::latest()->take(10)->get();
            foreach($articles as $article) {
                $item =['title' => $article->title, 'pubdate' =>$article->created_at,'link' => url($article->path()), 'description' => $article->body, 'author'=> $article->user->name];
                $feed->addItem($item);
            }
        }

        return $feed->render('rss');
    }
}
