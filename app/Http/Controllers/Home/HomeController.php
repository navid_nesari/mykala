<?php

namespace App\Http\Controllers\Home;

use App\Models\Course;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use Artesaos\SEOTools\Facades\SEOTools;

class HomeController extends Controller
{
    public function index()
    {
        SEOTools::setTitle('نوید سئو');

        /*
         cache()->put('name', 'navid', now()->addMinutes(3));
         cache()->store('file')->put('name', 'navid', 3);
         return cache()->get('name');

         cache()->pull('courses');
         cache()->pull('articles');
        */

        if(cache()->has('articles')){
            $articles = cache('articles');
        }else {
            $articles = Article::latest()->take(5)->get();
            cache(['articles' => $articles], now()->addMinutes(3));
        }

        if(cache()->has('courses')){
            $courses = cache('courses');
        }else{
            $courses = Course::latest()->take(3)->get();
            cache(['courses' => $courses], now()->addMinutes(3));
        }

        return view('home.index', compact('articles', 'courses'));
    }


    public function comment()
    {
        // dd($articleSlug);
        request()->validate([
            'comment' => ['required']
        ],[
           'comment.required' => 'فیلد مورد نظر الزامی است.',
        ]);

        $data = [
            'user_id' => auth()->user()->id,
            'parent_id' => request('parent_id'),
            'commentable_id' => request('commentable_id'),
            'commentable_type' => request('commentable_type'),
            'comment' => request('comment'),
        ];

        $comment = Comment::create($data);
        if($comment instanceof Comment) {
            return redirect()->back();
        }
    }
}
