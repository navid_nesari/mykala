<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class SitemapController extends Controller
{
    public function index()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('laravel.sitemap', 60);

        if( !$sitemap->isCached() ) {
            // $sitemap->add(url()->to('/home/sitmap-articles'), '2024-02-23T15:11:00+02:00', '0.9', 'daily');
            $sitemap->add(URL::to('/home/sitmap-articles'), '2024-02-23T15:11:00+02:00', '0.9', 'daily');
        }

        return $sitemap->render();
    }


    public function articles()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('laravel.sitemap.articles', 60);

        if( !$sitemap->isCached() ) {
            $articles = Article::latest()->get();
            foreach($articles as $article) {
                $sitemap->add(url()->to($article->path()), $article->created_at, '0.9', 'weekly');
            }
        }

        return $sitemap->render();
    }
}
