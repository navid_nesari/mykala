<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class CoursesController extends Controller
{
    public function single(Course $course)
    {
        // Redis::incr("view_count.{$course->id}.courses");
        $course->increment('view_count');
        $comments = $course->comments()->where('approved', 1)->where('parent_id', 0)->latest()
            ->with(['comments' => function($query) {
                $query->where('approved', 1)->latest();
        }])->get();

        return view('home.courses', compact('course', 'comments'));
    }
}
