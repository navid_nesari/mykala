<?php

namespace App\Http\Controllers\Home;

use Exception;
use App\Models\Learn;
use App\Models\Course;
use App\Models\Payment;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Shetabit\Multipay\Invoice;
use App\Http\Controllers\Controller;
use App\Helpers\PaymentGateways\zarinpal;
use Shetabit\Payment\Facade\Payment as FacadePayment;

class PaymentsController extends Controller
{
    public function zarinpalPayment()
    {
        request()->validate([
            'course_id' => ['required']
        ]);

        $course = Course::findOrFail(request('course_id'));

        if( auth()->user()->checkLearning($course) ) {
            return redirect()->back()->with('exist', 'دوره موردنظر قبلا خریداری شده.');
        }

        if( $course->price == 0 && $course->type == 'vip' ) {
            return back()->with('error', 'دوره مورد نظر قابل خریداری نمی باشد.');
        }

        $MerchantID 	= "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
        $Amount 		= $course->price;
        $Description 	= "تراکنش زرین پال";
        $Email 			= auth()->user()->email;
        $Mobile         = "";
        $CallbackURL 	= route('home.course.payment.checker');
        $ZarinGate 		= false;
        $SandBox 		= true;

        $zp 	= new zarinpal();
        $result = $zp->request($MerchantID, $Amount, $Description, $Email, $Mobile, $CallbackURL, $SandBox, $ZarinGate);

        if (isset($result["Status"]) && $result["Status"] == 100)
        {
            auth()->user()->payments()->create([
                'resnumber' => $result["Authority"],
                'course_id' => $course->id,
                'price'     => $course->price,
            ]);

            // Success and redirect to pay
            $zp->redirect($result["StartPay"]);
        } else {
            // error
            echo "خطا در ایجاد تراکنش";
            echo "<br />کد خطا : ". $result["Status"];
            echo "<br />تفسیر و علت خطا : ". $result["Message"];
        }
    }


    public function zarinpalChecker()
    {
        $authority = request()->Authority;
        $payment   = Payment::where('resnumber', $authority)->firstOrFail();
        $course    = Course::findOrFail($payment->course_id);

        $MerchantID 	= "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
        $Amount 		= $course->price;
        $ZarinGate 		= false;
        $SandBox 		= true;

        $zp 	= new zarinpal();
        $result = $zp->verify($MerchantID, $Amount, $SandBox, $ZarinGate);

        if (isset($result["Status"]) && $result["Status"] == 100)
        {
            if( $this->addUserForLearning($payment, $course) ) {
                return redirect($course->path())->with('success', 'دوره مورد نظر با موفقیت خریداری شد.');
            }
            // Success
            echo "تراکنش با موفقیت انجام شد";
            echo "<br />مبلغ : ". $result["Amount"];
            echo "<br />کد پیگیری : ". $result["RefID"];
            echo "<br />Authority : ". $result["Authority"];
        } else {
            // error
            echo "پرداخت ناموفق";
            echo "<br />کد خطا : ". $result["Status"];
            echo "<br />تفسیر و علت خطا : ". $result["Message"];
        }
    }


    protected function addUserForLearning($payment, $course)
    {
        $payment->update([
            'payment' => 1
        ]);

        Learn::create([
            'user_id' => auth()->user()->id,
            'course_id' => $course->id,
        ]);

        return true;
    }


    public function paypingPayment()
    {
        request()->validate([
            'course_id' => ['required']
        ]);

        $course = Course::findOrFail(request('course_id'));

        $token = "توکن اختصاصی ";
        $args = [
            "amount" =>  1000,
            "payerName" => auth()->user()->name,
            "description" => "توضیحات",
            "returnUrl" => route('home.course.payment.checker'),
            "clientRefId" => "شماره فاکتور"
        ];

        $payment = new \PayPing\Payment($token);

        try {
            $payment->pay($args);
        } catch (Exception $e) {
            throw $e;
        }
        //echo $payment->getPayUrl();

        return $payment->getPayUrl();


        // $transactionId = Str::random();
        // $invoice = (new Invoice)->amount(1000);
        // return FacadePayment::purchase($invoice, function($driver, $transactionId) {
            // We can store $transactionId in database.
        // })->pay()->render();

    }
}
