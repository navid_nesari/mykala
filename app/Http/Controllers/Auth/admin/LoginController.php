<?php

namespace App\Http\Controllers\Auth\admin;

use App\Events\UserActivation;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.admin.login');
    }


    public function login()
    {
        // validate request
        request()->validate([
            'email' => ['required', 'email', 'exists:users'],
            'password' => ['required', 'min:8']
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'email.email' => 'فیلد مورد نظر باید از نوع ایمیل باشد.',
            'email.exists' => 'ایمیل مورد نظر موجود نمیباشد.',
            'password.min' => 'فیلد مورد نظر حداقل باید 8 کارکتر باشد.',
        ]);

        $user = User::where('email', request('email'))->first();

        // hash check & login
        if($user instanceof User) {
            if($user->active == 0) {
                $checkActiveCode = $user->activationcodes()->where('expire', '>=', Carbon::now())->latest()->first();
                if( !is_null($checkActiveCode) ) {
                    if($checkActiveCode->expire > Carbon::now()) {
                        return redirect()->back()->withErrors(['code' => 'کد فعالسازی برای شما ایمیل شده لطفا بعداز 15 دقیقه دوباره لارگین کنید']);
                    }
                }
                event(new UserActivation($user));
            }

            if(Hash::check(request('password'), $user->password)) {
                Auth::guard('web')->login($user);
                return redirect()->route('admin.dashboard')->with('success', 'عملیات مورد نظر با موفقیت انجام شد.');
            }
            return redirect()->back()->with('warning', 'اطلاعات نا معتبر.');

            // or

            // if(Auth::attempt(request()->only('email', 'password'))) {
            //     return redirect()->route('admin.dashboard');
            // }

            // return redirect()->back();
        }
        return redirect()->back();
    }


    public function logout()
    {
        auth()->guard('web')->logout();

        return redirect()->route('auth.admin.login.form');
    }
}
