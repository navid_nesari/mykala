<?php

namespace App\Http\Controllers\Auth\admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;

class SocialLoginController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }


    public function handleProviderCallback()
    {
        try {
            $googleUser = Socialite::driver('google')->user();
            $user = User::where('email', $googleUser->email)->first();

            if (!$user) {
                $user = User::create([
                    'name' => $googleUser->name,
                    'email' => $googleUser->email,
                    'password' => Hash::make($googleUser->id),
                ]);
            }

            if($user->active == 0) {
                $user->active = 1;
                $user->save();
            }
            Auth::guard('web')->login($user);
            return redirect()->route('home');

        } catch (InvalidStateException $e) {

            $googleUser = Socialite::driver('google')->stateless()->user();
            $user = User::where('email', $googleUser->email)->first();

            if (!$user) {
                $user = User::create([
                    'name' => $googleUser->name,
                    'email' => $googleUser->email,
                    'password' => Hash::make($googleUser->id),
                ]);
            }

            if($user->active == 0) {
                $user->active = 1;
                $user->save();
            }
            Auth::guard('web')->login($user);

            return redirect()->route('home');
        }
    }
}
