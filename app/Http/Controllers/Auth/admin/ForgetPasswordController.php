<?php

namespace App\Http\Controllers\Auth\admin;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ResetPassword as MailResetPassword;
use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgetPasswordController extends Controller
{
    public function showForgetPasswordForm()
    {
        return view('auth.passwords.email-reset');
    }


    public function submitForgetPassword(Request $request)
    {
        request()->validate([
            'email' => ['required', 'email', 'exists:users,email']
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'email.email' => 'فیلد مورد نظر باید از نوع ایمیل باشد.',
            'email.exists' => 'ایمیل مورد نظر موجود نمیباشد.',
        ]);

        $token = Str::random(60);
        DB::table('password_resets')->insert([
            'email' => request('email'),
            'token' => $token,
            'created_at' =>Carbon::now()
        ]);

        /*
        $reset_link = route('auth.admin.reset-password.form', ['token' => $token, 'email' => request('email')]);
        $body = "برای تنظیم مجدد رمز عبور خود در سایت " . config('app.name') ."با ایمیل ". request('email'). "روی لینک زیر کلیک کنید.";

        Mail::send('auth.passwords.email-forget', ['reset_link' => $reset_link, 'body' => $body], function($massage){
            $massage->from('n.nesari1373@gmail.com', config('app.name'));
            $massage->to(request('email'), 'pepole')->subject('Reset Password');
        });
        */
        Mail::to(request('email'), 'pepole')->send(new MailResetPassword($token, request('email')));
        
        return redirect()->back()->with(['success' => 'لینک تنظیم مجدد رمز عبور برای شما ایمیل شده است.']);
    }


    public function showResetPasswordForm(string $token)
    {
        return view('auth.passwords.password-reset', ['token' => $token, 'email' => request('email')]);
    }


    public function submitResetPassword()
    {
        request()->validate([
            'email' => ['required', 'email', 'exists:users,email'],
            'password' => ['required', 'min:5', 'confirmed'],
            'password_confirmation' => ['required', 'min:5', /** 'same:password' */]
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'email.email' => 'فیلد مورد نظر باید از نوع ایمیل باشد.',
            'email.exists' => 'ایمیل مورد نظر موجود نمیباشد.',
            '*.min' => 'رمز عبور حداقل باید 5 کارکتر باشد.',
            'password.confirmed' => 'تایید رمز عبور باید مشابه رمز عبور باشد.',
            // 'password_confirmation.same' => 'تایید رمز عبور باید مشابه رمز عبور باشد.',
        ]);

        $check_token = DB::table('password_resets')->where([
            'email' => request('email'),
            'token' => request('token')
        ])->first();

        if( !$check_token ) {
            return redirect()->back()->with('fail', 'توکن معتبر نمیباشد.');
        }
        User::where( 'email', request('email') )->update([
            'password' => Hash::make(request('password'))
        ]);
        DB::table('password_resets')->where('email', request('email'))->delete();

        return redirect()->route('auth.admin.login')->with('info', 'رمز عبور شما با موفقیت به روز رسانی شد اکنون شما میتوانید با رمز عبور جدید وارد شوید.');
    }
}
