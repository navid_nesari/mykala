<?php

namespace App\Http\Controllers\Auth\app;

use App\Events\UserActivation;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function showRegisterForm()
    {
        return view('auth.app.register');
    }


    public function register()
    {
        // validation
        request()->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:8']
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'email.email' => 'فیلد مورد نظر باید از نوع ایمیل باشد.',
            'email.unique' => 'ایمیل مورد نظر قبلا ثبت شده.',
            'password.min' => 'فیلد مورد نظر حداقل باید 8 کارکتر باشد.',
        ]);

        // create
        $data = [
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            // 'password' => Hash::make(request('password')),
        ];
        $user = User::create($data);

        event(new UserActivation($user));
        if($user instanceof User) {

            Auth::guard('web')->login($user);
            return redirect()->route('home');
        }
        return redirect()->back();
    }
}
