<?php

namespace App\Http\Controllers\Auth\app;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.app.login');
    }


    public function login()
    {
        //validation
        request()->validate([
            'email' => ['required', 'email', 'exists:users'],
            'password' => ['required', 'min:8'],
        ],[
            '*.required' => 'فیلد مورد نظر الزامی است.',
            'email.email' => 'فیلد مورد نظر باید از نوع ایمیل باشد.',
            'email.exists' => 'ایمیل مورد نظر موجود نمیباشد.',
            'password.min' => 'فیلد مورد نظر حداقل باید 8 کارکتر باشد.',
        ]);

        $user = User::where('email', request('email'))->first();
        if($user instanceof User) {
            if(Hash::check(request('password'), $user->password)) {
                Auth::guard('web')->login($user);
                return redirect()->route('');
            }
            return redirect()->back();
        }
        return redirect()->back();
    }


    public function logout()
    {
        Auth::guard('web')->logout();

        return redirect()->route('auth.app.login.form');
    }
}
