<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ArticleEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // کانال خصوصی
        return new PrivateChannel('articles.admin');

        // کانال عمومی
        // return new Channel('articles');
    }

    /*
    |----------------------------------------------------------------------------
    | : broadcastWith() نکات
    |----------------------------------------------------------------------------
    | از این متد زمانی استفاده میشود که بخواهیم پیغام شخصی خودمان را به جای
    | شده از متد سازنده کلاس ایونت نشان دهیم return نتیجه
    |
    */

    // public function broadcastWith()
    // {
    //     return [
    //         'message' => 'this my broadcast.'
    //     ];
    // }
}
