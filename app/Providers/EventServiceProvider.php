<?php

namespace App\Providers;

use App\Events\ArticleEvent;
use App\Events\UserActivation;
use App\Listeners\UserActivation\SendMailNotification ;
use App\Listeners\UserActivation\SendSMSNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserActivation::class => [
            SendMailNotification::class,
            SendSMSNotification::class,
        ],
        ArticleEvent::class =>[
            //
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
