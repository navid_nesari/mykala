<?php

namespace App\Providers;

use App\Models\Comment;
use App\Models\Payment;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('admin.partials.sidebar', function($view) {
            $successful = Comment::where('approved', 1)->count();
            $unsuccessful = Comment::where('approved', 0)->count();
            $successfulPayments = Payment::where('payment', 1)->count();
            $unsuccessfulPayments = Payment::where('payment', 0)->count();
            
            $view->with(compact(
                'successful',
                'unsuccessful',
                'successfulPayments',
                'unsuccessfulPayments'
            ));
        });

        // view()->composer('admin.partials.sidebar', function($view) {

        //     $view->with(['unsuccessful' => $unsuccessful]);
        // });
    }
}
