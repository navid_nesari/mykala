<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        \App\Models\Article::class => \App\Policies\ArticlePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /*
         Gate::define('show-comment', function($user){
             return $user->hasRole(Permission::where('name', 'show-comment')->first()->roles);
         });
        */
        foreach($this->getPermissions() as $permission) {
            Gate::define($permission->name, function($user) use($permission){
                return $user->hasRole($permission->roles);
            });
        }
    }

    protected function getPermissions() {

        return Permission::get();
        // return Permission::with('roles')->get();
    }
}
